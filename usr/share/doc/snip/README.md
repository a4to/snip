# Snip Snippet Editor

Snip is a command-line utility that allows users to create and edit snippets for various programming
languages and file types. It provides a convenient interface for managing snippets and organizing them by category and
priority.

## Installation

To install the Snip Snippet Editor, follow these steps:

1. Clone the repository:

   ```shell
   git clone https://gitlab.com/a4to/snip.git
   ```

2. Change to the snip directory:

   ```shell
   cd snip
   ```

3. Run the install script:

   ```shell
   make install
   ```

   The Snip Snippet Editor and its dependencies will be installed.

4. Add the following code snippet to your Vim configuration file (`init.vim` or `vimrc`):

```vim
" ---------------------------
"   AutoComplete Snippets:
" ---------------------------

" This code snippet ensures that all AutoFill (auto-completion) snippets,
" are automatically expanded when the snippet is triggered.

function! AutoCompleteSnippets()
    let l:line_number = 1
    while l:line_number <= line('$')
        let l:line = getline(l:line_number)
        if l:line =~ '^snippet'
            let l:autocompletion_line = l:line

            if l:autocompletion_line =~ ' "$'
                let l:autocompletion_line = l:autocompletion_line . ' '
                call setline(l:line_number, l:autocompletion_line)
            endif

            if l:autocompletion_line !~ 'A$' && l:autocompletion_line !~# '" .*\C\ca.*'
                if l:autocompletion_line !~ '"$'
                    let l:autocompletion_line = l:autocompletion_line . ' "" A'
                else
                    let l:autocompletion_line = l:autocompletion_line . 'A'
                endif
                call setline(l:line_number, l:autocompletion_line)
            endif

            if l:autocompletion_line =~ '"A'
                let l:autocompletion_line = substitute(l:autocompletion_line, '"A', '" A', 'g')
                call setline(l:line_number, l:autocompletion_line)
            endif
        endif
        let l:line_number += 1
    endwhile
endfunction

augroup AutoCompleteSnippetsOnSave
    autocmd!
    autocmd BufWritePre AutoFill-p1.snippets call AutoCompleteSnippets()
augroup END

" ---------------------------
" Specify Snippet Directory:
" ---------------------------

" Ensure you specify your snippet directory in your config file, e.g:

let g:UltiSnipsSnippetsDir = expand('$HOME/.config/nvim/snipDir')
let g:UltiSnipsSnippetDirectories=['UltiSnips', expand('$HOME/.config/nvim/snipDir')]

```

   This code snippet should be placed in your Vim configuration file (e.g., `init.vim` or `vimrc`). It defines a function
   `AutoCompleteSnippets()` that is triggered when saving a file named `AutoFill-p1.snippets`. It adds automatic
   autocompletion for snippets by modifying the snippet lines that start with `snippet` and appending `A` after the
   snippet trigger. This enables faster and more convenient snippet expansion in Vim.

5. Save and reload your Vim configuration file.

## Usage

To launch snip, simply run the snip:

```shell
snip
```

This will open the graphical interface using `dialog` where you can perform various operations, such as creating new
snippet categories and editing existing snippets.

## Contributing

Contributions are welcome! If you encounter any issues or have suggestions for improvements, please submit a bug report
or open a pull request on the [snip repository](https://gitlab.com/a4to/snip).

## License

The Snip Snippet Editor is open-source software licensed under the [MIT license](https://gitlab.com/a4to/snip/-/blob/master/usr/share/licenses/snip/LICENSE).

