PKGNAME = snip
PKGVER = 1.2.0
PKGREL = 1
ARCH = any

all: package

package:
	@install -Dm755 usr/bin/$(PKGNAME) $(PKGNAME)-$(PKGVER)-$(PKGREL)-$(ARCH)/usr/bin/$(PKGNAME)
	@install -Dm644 usr/share/licenses/$(PKGNAME)/LICENSE $(PKGNAME)-$(PKGVER)-$(PKGREL)-$(ARCH)/usr/share/licenses/$(PKGNAME)/LICENSE
	@install -Dm644 usr/share/doc/$(PKGNAME)/README.md $(PKGNAME)-$(PKGVER)-$(PKGREL)-$(ARCH)/usr/share/doc/$(PKGNAME)/README.md
	@install -Dm644 usr/share/man/man1/$(PKGNAME).1.gz  $(PKGNAME)-$(PKGVER)-$(PKGREL)-$(ARCH)/usr/share/man/man1/$(PKGNAME).1.gz

install:
	@install -Dm755 $(PKGNAME)-$(PKGVER)-$(PKGREL)-$(ARCH)/usr/bin/$(PKGNAME) /usr/bin/$(PKGNAME)
	@install -Dm644 $(PKGNAME)-$(PKGVER)-$(PKGREL)-$(ARCH)/usr/share/licenses/$(PKGNAME)/LICENSE /usr/share/licenses/$(PKGNAME)/LICENSE
	@install -Dm644 $(PKGNAME)-$(PKGVER)-$(PKGREL)-$(ARCH)/usr/share/doc/$(PKGNAME)/README.md /usr/share/doc/$(PKGNAME)/README.md
	@install -Dm644 $(PKGNAME)-$(PKGVER)-$(PKGREL)-$(ARCH)/usr/share/man/man1/$(PKGNAME).1.gz /usr/share/man/man1/$(PKGNAME).1.gz

clean:
	@rm -f /usr/bin/$(PKGNAME)
	@rm -f /usr/share/licenses/$(PKGNAME)/LICENSE
	@rm -f /usr/share/doc/$(PKGNAME)/README.md
	@rm -f /usr/share/man/man1/$(PKGNAME).1.gz

.PHONY: all package install clean
